﻿using System.Collections;
using UnityEngine;

public class ThirdPCamera : MonoBehaviour
{
    public const float Y_ANGLE_MIN = 0f;
    public const float Y_ANGLE_MAX = 50f;

    public Transform player;
    public Transform camTransform;

    private Camera cam;

    public float distance = 10f;
    public float currentX = 0f;
    public float currentY = 0f;
    public float sensivityX = 4f;
    public float sensivityY = 1f;


    void Start()
    {
        camTransform = transform;
        cam = Camera.main;
    }

    // Update is called once per frame
    void Update()
    {
        currentX += Input.GetAxis("Mouse X");
        currentY -= Input.GetAxis("Mouse Y");

        currentY = Mathf.Clamp(currentY, Y_ANGLE_MIN, Y_ANGLE_MAX);
    }

    void FixedUpdate()
    {
        Vector3 dir = new Vector3(0, 0, -distance);
        Quaternion rotation = Quaternion.Euler(currentY, currentX, 0);
        player.rotation = Quaternion.Euler(0, currentX, 0);
        camTransform.position = player.position + rotation * dir;

        camTransform.LookAt(player.position);
    }
}
