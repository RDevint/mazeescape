﻿using System.Collections.Generic;
using UnityEngine;

public class MazeDataGenerator
{
    public float placementThreshold;    // шанс того, что клетка будет проходимой
    
    //madness
    //private int current = 0;    //current row or column
    public int gapSize = 2; //Минимаьный размер прохода
    public int orientation = 1;
    
    public int iter = 50;
    
    public MazeDataGenerator()
    {
        //Значение от 0 до 1.
        placementThreshold = .6f;
    }
    
    private int chooseOrientation(int width, int height)
    {
        if(width < height)
        {
            return 1;   //horizontal
        }
        else
        {
            return 0;   //vertical
        }
    }
    //orientation 0 - vertical, 1 - horizontal
   void divideMap(int[,] grid, int x, int y, int width, int height, int gap, int orientation)
    {
       if(width > gap && height > gap && iter > 0)
       {
       //await Task.Delay(TimeSpan.FromSeconds(1));
       
       iter--;
       
        bool horizontal = orientation == 1;
        
        //Начальная точка для стены
        int wx = x + (horizontal ? 0 : Random.Range(1, width-2));
        int wy = y + (horizontal ? Random.Range(1, height-2) : 0);
        
        //Надальная точка для прохода в стене
        int px = wx + (horizontal ? Random.Range(0, width-gap-1) : 0);
        int py = wy + (horizontal ? 0 : Random.Range(0, height-gap-1));
        
        // what direction will the wall be drawn?
        int dx = horizontal ? 1 : 0;
        int dy = horizontal ? 0 : 1;
        
        //how long will the wall be?
        int length = horizontal ? width : height;
        
        //fill it with wall and passage
        while (length > 0)
        {
            if( (horizontal && wx >= px && wx <= (px + gap-1)) || (!horizontal && wy >= py && wy <= (py + gap-1)) )
            {
                grid[wx, wy] = 0;
            }
            else
            {
                grid[wx, wy] = 1;
            }
            
            wx += dx;
            wy += dy;

            length--;
        };
        
        //recursive drawing in same starting point (x,y) areas
        int nx = x;
        int ny = y;
        int w, h;
        if (horizontal)
        {
            w = width;
            h = wy-y;
        }
        else
        {
            w = wx-x;
            h = height;
        }
        divideMap(grid, nx, ny, w, h, gap, chooseOrientation(w, h));
        
        //recursive drawing in other starting point (x0,y0) areas
        int nx0, ny0;
        int w0, h0;
        if (horizontal)
        {
            nx0 = x;
            ny0 = wy+1;
            w0 = width;
            h0 = height-1 - wy-y;
        }
        else
        {
            nx0 = wx+1;
            ny0 = y;
            w0 = width-1 - wx-x;
            h0 = height;
        }
        divideMap(grid, nx0, ny0, w0, h0, gap, chooseOrientation(w0, h0));
       }
    }

    //stub to fill in
    public int[,] FromDimensions(int sizeRows, int sizeCols)
    {
        int[,] maze = new int[sizeRows, sizeCols];
        
        divideMap(maze, 0, 0, sizeCols, sizeRows, gapSize, orientation);
        
        return maze;
    }
}