﻿using UnityEngine;
using UnityEditor;

//[ExecuteInEditMode]
public class DrawSolidArc : MonoBehaviour
{
    public float shieldArea = 7;
    //DrawSolidArc t = target as DrawSolidArc;

    void OnGUI()
    {
        Handles.DrawSolidArc(transform.position, transform.up, -transform.right,
                                110, shieldArea);
    }
}
