﻿using System.Collections.Generic;
using UnityEngine;

public class MazeMeshGenerator
{
    //Параметры генерации проходов
    public float width;         //Ширина проходов в лабиринте
    public float height;        //Высота стен лабиринта

    //Конструктор по умолчанию
    public MazeMeshGenerator()
    {
        width = 3.75f;
        height = 3.5f;
    }

    public Mesh FromData(int[,] data)
    {
        Mesh maze = new Mesh();

        //the vertices, the UV coordinates and the triangles
        List<Vector3> newVertices = new List<Vector3>();
        List<Vector2> newUVs = new List<Vector2>();

        // List<int> newTriangles = new List<int>();
        maze.subMeshCount = 2;
        List<int> floorTriangles = new List<int>();
        List<int> wallTriangles = new List<int>();

        int rMax = data.GetUpperBound(0);
        int cMax = data.GetUpperBound(1);
        float halfH = height * .5f;

        /*Поклеточно отрисовываем пол, стены и прочее (если нужно)
         *Переменная i это x, а j это z
         *Считаем, что поле в редакторе Unity расположено осью z вверх, а осью x - направо
         */
        for (int i = 0; i <= rMax; i++)
        {
            for (int j = 0; j <= cMax; j++)
            {
                //Рисуем пол и стены внутри игрового поля
                if (data[i, j] == 0)
                {
                    AddQuad(Matrix4x4.TRS(
                         new Vector3(i * width, 0, j * width),
                         Quaternion.LookRotation(Vector3.up),
                         new Vector3(width, width, 1)
                         ), ref newVertices, ref newUVs, ref floorTriangles);

                    //Если слева стена от данной пустой точки
                    if (i - 1 < 0 || data[i - 1, j] == 1)
                    {
                        AddQuad(Matrix4x4.TRS(
                            new Vector3((i - .5f) * width, halfH, j * width),
                            Quaternion.LookRotation(Vector3.right),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                    }

                    //Если сверху стена...
                    if (j + 1 > cMax || data[i, j + 1] == 1)
                    {
                        AddQuad(Matrix4x4.TRS(
                            new Vector3(i * width, halfH, (j + .5f) * width),
                            Quaternion.LookRotation(Vector3.back),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                    }

                    //Если снизу стена...
                    if (j - 1 < 0 || data[i, j - 1] == 1)
                    {
                        AddQuad(Matrix4x4.TRS(
                            new Vector3(i * width, halfH, (j - .5f) * width),
                            Quaternion.LookRotation(Vector3.forward),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                    }

                    //Если справа стена...
                    if (i + 1 > rMax || data[i + 1, j] == 1)
                    {
                        AddQuad(Matrix4x4.TRS(
                            new Vector3((i + .5f) * width, halfH, j * width),
                            Quaternion.LookRotation(Vector3.left),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                    }
                }
                else if (data[i, j] == 1)
                {
                    AddQuad(Matrix4x4.TRS(
                         new Vector3(i * width, height, j * width),
                         Quaternion.LookRotation(Vector3.up),
                         new Vector3(width, width, 1)
                         ), ref newVertices, ref newUVs, ref wallTriangles);
                }

                //Создаем внешнюю левую стену
                if(i == 0)
                {
                    AddQuad(Matrix4x4.TRS(
                            new Vector3((i - .5f) * width, halfH, j * width),
                            Quaternion.LookRotation(Vector3.left),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                }
                //Создаем внешнюю правую стену
                if (i == rMax)
                {
                    AddQuad(Matrix4x4.TRS(
                            new Vector3((i + .5f) * width, halfH, j * width),
                            Quaternion.LookRotation(Vector3.right),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                }
                //Создаем внешнюю нижнюю стену
                if (j == 0)
                {
                    AddQuad(Matrix4x4.TRS(
                            new Vector3(i * width, halfH, (j - .5f) * width),
                            Quaternion.LookRotation(Vector3.back),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                }
                //Создаем внешнюю верхнюю стену
                if (j == cMax)
                {
                    AddQuad(Matrix4x4.TRS(
                            new Vector3(i * width, halfH, (j + .5f) * width),
                            Quaternion.LookRotation(Vector3.forward),
                            new Vector3(width, height, 1)
                        ), ref newVertices, ref newUVs, ref wallTriangles);
                }
            }
        }

        //Формируем квадрат
        maze.vertices = newVertices.ToArray();
        maze.uv = newUVs.ToArray();

        //maze.triangles = newTriangles.ToArray();
        maze.SetTriangles(floorTriangles.ToArray(), 0);
        maze.SetTriangles(wallTriangles.ToArray(), 1);

        maze.RecalculateNormals();

        return maze;
    }

    private void AddQuad(Matrix4x4 matrix, ref List<Vector3> newVertices, ref List<Vector2> newUVs, ref List<int> newTriangles)
    {
        int index = newVertices.Count;

        //Углы квадрата
        Vector3 vert1 = new Vector3(-.5f, -.5f, 0);
        Vector3 vert2 = new Vector3(-.5f, .5f, 0);
        Vector3 vert3 = new Vector3(.5f, .5f, 0);
        Vector3 vert4 = new Vector3(.5f, -.5f, 0);

        //Добавляем все verticles в лист
        newVertices.Add(matrix.MultiplyPoint3x4(vert1));
        newVertices.Add(matrix.MultiplyPoint3x4(vert2));
        newVertices.Add(matrix.MultiplyPoint3x4(vert3));
        newVertices.Add(matrix.MultiplyPoint3x4(vert4));

        //Задаем UV
        newUVs.Add(new Vector2(1, 0));
        newUVs.Add(new Vector2(1, 1));
        newUVs.Add(new Vector2(0, 1));
        newUVs.Add(new Vector2(0, 0));

        //Верхняя половина квадрата, сформированная из вершин с индексами 0, 1 и 2 из списка вершин
        newTriangles.Add(index+2);
        newTriangles.Add(index+1);
        newTriangles.Add(index);

        //Нижняя половина квадрата из вершин с индексами 0, 2 и 3 из списка вершин
        newTriangles.Add(index+3);
        newTriangles.Add(index+2);
        newTriangles.Add(index);
    }
}
