﻿using UnityEngine;
using UnityEngine.AI;

public class MazeConstructor : MonoBehaviour
{
    public NavMeshSurface surface;

   //Для дебага
	public bool showDebug;
	//Материалы для игрового поля
	[SerializeField] private Material mazeMat1;
	[SerializeField] private Material mazeMat2;
	[SerializeField] private Material startMat;
	[SerializeField] private Material treasureMat;
	
	//public int rows = 1, cols = 1;
	//Генератор игрового поля
	private MazeDataGenerator dataGenerator;
    private MazeMeshGenerator meshGenerator;
	
	//Массив клеток игрового поля
	public int[,] data
	{
		get; private set;
	}
	
	//
	void Awake()
	{
		dataGenerator = new MazeDataGenerator();
        meshGenerator = new MazeMeshGenerator();

		//data = new int[rows, cols];
		
	}
	
	//Функция генерации нового игрового поля(лабиринта)
	public void GenerateNewMaze(int sizeRows, int sizeCols)
	{
		data = dataGenerator.FromDimensions(sizeRows, sizeCols);
        DisplayMaze();
	}
	
	void OnGUI()
	{
		//Отображние лабиринта. Только для дебага
		if(showDebug)
		{
			//Копируем и подготавливаем данные для отображения
			int [,] maze = data;
			int rMax = maze.GetUpperBound(0);
			int cMax = maze.GetUpperBound(1);
			
			string msg = "";
			
			/*Проходимся по массиву и сопостовляем символы для отображения кеток поля.
			 *.... для пустых (0) полей и == для стен(1)
             *Идем считая, что координаты увеличиваются слева-направо и снизу - вверх
			 */
			 for (int i = 0; i <= rMax; i++)
			 {
			 	for (int j = 0; j <= cMax; j++)
			 	{
			 		if (maze[i, j] == 0)
			 		{
			 			msg += "....";
			 		}
			 		else if (maze[i, j] == 1)
			 		{
			 			msg += "==";
			 		}
			 	}
			 	msg += "\n";
			 }
			 
			 //Поле отображения лабиринта для дебага
			 GUI.Label(new Rect(20, 20, 500, 500), msg);
		}
	}

    //Отобразить сгенерированное поле
    private void DisplayMaze()
    {
        GameObject go = new GameObject();
        go.transform.position = Vector3.zero;
        go.name = "Procedural Maze";
        go.tag = "Generated";

        MeshFilter mf = go.AddComponent<MeshFilter>();
        mf.mesh = meshGenerator.FromData(data);

        MeshCollider mc = go.AddComponent<MeshCollider>();
        mc.sharedMesh = mf.mesh;

        MeshRenderer mr = go.AddComponent<MeshRenderer>();
        mr.materials = new Material[2] { mazeMat1, mazeMat2 };

        surface.BuildNavMesh();
    }
}
