﻿using System;
using UnityEngine;

//Заставляем добавлять MazeConstructor вместе с эти скриптом
[RequireComponent (typeof(MazeConstructor))]
public class GameController : MonoBehaviour
{
    private MazeConstructor generator;
    public int rows = 10, cols = 10;
    // Start is called before the first frame update
    void Start()
    {
        generator = GetComponent<MazeConstructor>();
        generator.GenerateNewMaze(rows, cols);
    }
}
